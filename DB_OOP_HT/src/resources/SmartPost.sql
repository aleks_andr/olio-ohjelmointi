
PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS "Packages"(
	"PackageID" INTEGER PRIMARY KEY,
	"deliveryClass" INTEGER CHECK("deliveryClass" in (1,2,3)) NOT NULL,

	FOREIGN KEY("deliveryClass") REFERENCES "DeliveryClasses"("deliveryClass")
);

CREATE TABLE IF NOT EXISTS "Routes" (
	"PackageID" INTEGER  PRIMARY KEY,
	"StartID" INTEGER NOT NULL,
	"DestID" INTEGER NOT NULL,
	CONSTRAINT packID FOREIGN KEY("PackageID") REFERENCES "Packages"("PackageID") ON DELETE CASCADE,
	CONSTRAINT sID FOREIGN KEY("StartID") REFERENCES "SmartPost"("PostID") ON DELETE CASCADE,
	CONSTRAINT dID FOREIGN KEY("DestID") REFERENCES "SmartPost"("PostID") ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS "SmartPost" (
	"PostID" INTEGER PRIMARY KEY,
	"name" VARCHAR(60) NOT NULL
);

CREATE TABLE IF NOT EXISTS "PostalCodes"(
	"postalcode" VARCHAR(5) PRIMARY KEY,
	"city" VARCHAR(60) NOT NULL
);

CREATE TABLE IF NOT EXISTS "Metadata"(
	"PostID" INTEGER PRIMARY KEY,
	"availability" VARCHAR(60) NOT NULL,

	CONSTRAINT pID FOREIGN KEY("PostID") REFERENCES "SmartPost"("PostID") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "Location"(
	"PostID" INTEGER PRIMARY KEY,
	"postalcode" VARCHAR(5) NOT NULL,
	"address" VARCHAR(60) NOT NULL,
	"lat" REAL NOT NULL,
	"lng" REAL NOT NULL,


	CONSTRAINT pID FOREIGN KEY("PostID") REFERENCES "SmartPost"("PostID") ON DELETE CASCADE,
	FOREIGN KEY("postalcode") REFERENCES "PostalCodes"("postalcode")
);

CREATE TABLE IF NOT EXISTS "Contains"(
	"ItemID" INTEGER NOT NULL,
	"PackageID" INTEGER NOT NULL,

	PRIMARY KEY ("ItemID", "PackageID"),
	CONSTRAINT fk_item FOREIGN KEY ("ItemID") REFERENCES "Items"("ItemID") ON DELETE CASCADE,
	CONSTRAINT fk_package FOREIGN KEY ("PackageID") REFERENCES "Packages"("PackageID") ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS "Items" (
	"ItemID" INTEGER PRIMARY KEY,
	"name" VARCHAR(60) NOT NULL,
	"weight" REAL NOT NULL,
	"volume" REAL NOT NULL,
	"fragile" BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS "DeliveryClasses"(
	"deliveryClass" INTEGER PRIMARY KEY CHECK("deliveryClass" IN (1,2,3)),
	"maxRange" INTEGER NOT NULL,
	"maxWeight" REAL NOT NULL,
	"maxVolume" REAL NOT NULL,
	"fragile" BOOLEAN NOT NULL
);


CREATE TABLE IF NOT EXISTS "EventTypes"(
	"EventTypeID" INTEGER PRIMARY KEY,
	"description" VARCHAR(60) NOT NULL
);

CREATE TABLE IF NOT EXISTS "EventLog"(
	"EventID" INTEGER PRIMARY KEY,
	"dateTime" DATETIME DEFAULT (datetime('now', 'localtime')),
	"EventTypeID" INTEGER NOT NULL,

	CONSTRAINT eID FOREIGN KEY ("EventTypeID") REFERENCES "EventTypes"("EventTypeID") ON DELETE CASCADE
);

INSERT OR IGNORE INTO "DeliveryClasses"("deliveryClass","maxRange", "maxWeight","maxVolume", "fragile") VALUES(1,150, 50, 10, 0);
INSERT OR IGNORE INTO "DeliveryClasses"("deliveryClass","maxRange", "maxWeight","maxVolume", "fragile") VALUES(2,10000000, 35, 7.5, 1);
INSERT OR IGNORE INTO "DeliveryClasses"("deliveryClass","maxRange", "maxWeight","maxVolume", "fragile") VALUES(3,10000000, 100, 20, 0);

INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (1, "Ohjelma käynnistetty.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (2, "Ohjelma suljettu.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (3, "Tietokantavirhe.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (4, "Luotu uusi paketti.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (5, "Luotu uusi esine.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (6, "Reitti piirretty.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (7, "Esine lisätty pakettiin.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (8, "Esine poistettu paketista.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (9, "Esine poistettu.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (10, "Paketti poistettu.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (11, "Loki tulostettu.");
INSERT OR IGNORE INTO "EventTypes"("EventTypeID", "description") VALUES (12, "Lokitiedoston tulostaminen epäonnistui.");




CREATE INDEX IF NOT EXISTS eventIndex ON EventLog (dateTime);

CREATE VIEW IF NOT EXISTS PostInfo AS SELECT SmartPost.PostID, SmartPost.name, Metadata.availability, Location.postalcode, PostalCodes.city, Location.address, Location.lat, Location.lng  FROM ((SmartPost INNER JOIN Location ON SmartPost.PostID = Location.PostID)  INNER JOIN Metadata ON SmartPost.PostID = Metadata.PostID) INNER JOIN PostalCodes ON Location.postalcode = PostalCodes.postalcode;
CREATE VIEW IF NOT EXISTS EventInfo AS SELECT EventLog.EventID, EventLog.EventTypeID, EventLog.dateTime, EventTypes.description FROM EventLog INNER JOIN EventTypes ON EventLog.EventTypeID = EventTypes.EventTypeID ORDER BY EventLog.dateTime ASC;


