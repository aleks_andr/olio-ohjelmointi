// Andrey Aleksandrov 0447608
package application;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

//Imports used for generating MD5 checksum
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.sqlite.SQLiteConfig;

//=======================================
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class DatabaseHandler {
	private static String dbFileName = "SmartPost.db";
	private static String sqlSchemaFile = "/resources/SmartPost.sql";
	private static String hash = "5d97c44553c810470507f016cb7a74a1";
	public static Connection conn;
	
	//Opens the connection to database file, or creates it
	//returns the Connection object
	public static Connection connect() throws SQLException{
		
		if(DatabaseHandler.conn == null || DatabaseHandler.conn.isClosed()){
			String url = "jdbc:sqlite:" + System.getProperty("user.home") + "/" + dbFileName;
			Connection conn = null;
			
			try{
				SQLiteConfig config = new SQLiteConfig();  
		        config.enforceForeignKeys(true); 
				conn = DriverManager.getConnection(url, config.toProperties());
				conn.setAutoCommit(false);
			}catch(Exception e){
				
				//e.printStackTrace();
				return null;
			}
			return conn;
		}else{
			return DatabaseHandler.conn;
		}
	}	
	
	//Checks the MD5 HashSum of the included SQL schema file to verify it has not been modified
	//Source: https://crunchify.com/how-to-get-md5-checksum-for-any-given-file-in-java-use-commons-codecs-digestutils-md5hex/
	private static boolean checkHashSum(){
		String value = null;
		InputStreamReader inStream = null;
		
		try{
			inStream = new InputStreamReader(DatabaseHandler.class.getResourceAsStream(sqlSchemaFile));
			
			value = DigestUtils.md5Hex(IOUtils.toByteArray(inStream, "UTF-8"));
			
			
		}catch(Exception e){
			e.printStackTrace();
			Alert alert  = new Alert(AlertType.ERROR);
			alert.setTitle("SmartPost.sql muokattu!");
			alert.setContentText("Ohjelman SQL-tiedostoa on muokattu, ohjelmaa ei ajeta.");
			alert.showAndWait();
			System.out.println("SQL schema checksum check failed.");
			return false;
		}finally{
			IOUtils.closeQuietly(inStream);
		}
		
		System.out.println("Hash: " + value);
		
		if(value.equals(hash)){
			System.out.println("Matching hash.");
			return true;
		}
		
		Alert alert  = new Alert(AlertType.ERROR);
		alert.setTitle("SmartPost.sql muokattu!");
		alert.setContentText("Ohjelman SQL-tiedostoa on muokattu, ohjelmaa ei ajeta.");
		alert.setResizable(true);
		alert.showAndWait();
		
		//This should always return false by default.
		//RETURN TRUE to bypass the check for debugging 
		return false;
		//=======================================//
		
	}
	

	//Constructs the database schema from a given .sql file
	public static void buildDb(){
		//Checks for validity of sql-schema to prevent editing of the file
		if(!checkHashSum()){
			System.out.println("Schema file has been modified, SQL injection possible, exiting...");
			Platform.exit();
		}
		
		
		try{
			DatabaseHandler.conn = connect();
			BufferedReader br = new BufferedReader(new InputStreamReader(DatabaseHandler.class.getResourceAsStream(sqlSchemaFile)));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null){
				sb.append(line);
			}
			
			String query = sb.toString();
			
			//System.out.println(query);
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(query);
			statement.close();
			conn.commit();
			conn.close();
			//System.out.println(res);
			br.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//adds new entry to SmartPost, Location and Metadata tables	
	public static void addPost(SmartPost obj){
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps1 = null;
			ps1 = conn.prepareStatement("INSERT INTO \"SmartPost\"(\"name\") VALUES (?);");
			System.out.println("Adding: "+ obj.getName());
			ps1.setString(1, obj.getName());
			ps1.executeUpdate();
			ps1.close();
			
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery("SELECT last_insert_rowid()");
			//System.out.println(rs.getInt(1));
			int curr_id = rs.getInt(1);
			
			PreparedStatement ps2 = null;
					
			ps2 = conn.prepareStatement(
					"INSERT INTO Metadata(PostID, availability) VALUES (?,?);"	
					);
			ps2.setInt(1, curr_id);
			ps2.setString(2, obj.getAvail());
			
			PreparedStatement ps3 = null;
			ps3 = conn.prepareStatement("INSERT INTO Location(PostID,postalcode,address,lat,lng) VALUES (?,?,?,?,?);");
			
			ps3.setInt(1, curr_id);
			ps3.setString(2, obj.getCode());
			ps3.setString(3, obj.getAddress());
			ps3.setDouble(4, obj.getLat());
			ps3.setDouble(5, obj.getLng());
			
			PreparedStatement ps4 = null;
			ps4 = conn.prepareStatement("INSERT OR IGNORE INTO PostalCodes(postalcode, city) VALUES (?,?) ");
			ps4.setString(1, obj.getCode());
			ps4.setString(2, obj.getCity());
			
			ps2.execute();
			ps2.close();
			ps4.execute(); //postalcodes inserted before location to meet FK constraints
			ps4.close();
			ps3.execute();
			ps3.close();
			
		}catch(Exception e){
			e.printStackTrace();
			MainController.pUIcontroller.alertFailedOperation();
		}
	}
	
	//Gets SmartPost information from database and returns ArrayList<SmartPost>
	public static ArrayList<SmartPost> getPostInfo(){

		ArrayList<SmartPost> lst = new ArrayList<SmartPost>();
		//Select from view
		String sql = "SELECT * FROM PostInfo;";
		
		try{
			DatabaseHandler.conn = connect();
			Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()){
				SmartPost sp = new SmartPost(rs.getString("name"), rs.getString("postalcode"), rs.getString("address"), rs.getString("city"), rs.getString("availability"), rs.getFloat("lat"), rs.getFloat("lng"));
				sp.setID(rs.getInt("PostID"));
				lst.add(sp);
			}
			System.out.println("SmartPostList: " + lst.size());
			return lst;
		}catch(Exception e){
			e.printStackTrace();
			MainController.pUIcontroller.alertFailedOperation();
			return null;
		}
		
	}
	
	//Fetch list of items from database and return an ArrayList<Item> object
	public static ArrayList<Item> getItemInfo(){
		ArrayList<Item> lst = new ArrayList<Item>();
		
		String sql = "SELECT * FROM Items;";
		
		try{
			DatabaseHandler.conn = connect();
			Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()){
				Item i = new Item(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4), rs.getBoolean(5));
				lst.add(i);
			}
			System.out.println("ItemList: " + lst.size());
			return lst;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	//Fetch delivery class restrictions
	public static ResultSet getClassRestrictions(int packClass){
		
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM DeliveryClasses WHERE deliveryClass = ?");
			ps.setInt(1, packClass);
			ResultSet rs = ps.executeQuery();
			return rs;
		}catch(Exception e){
			MainController.pUIcontroller.alertFailedOperation();
			e.printStackTrace();
			return null;
		}
	}
	
	//Gets all packages from the Packages table and return a list of Package-objects
	public static ArrayList<Package> getPackages(){
		ArrayList<Package> packList = new ArrayList<Package>();
		
		try{
			DatabaseHandler.conn = connect();
			Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM Packages;");
			
			while(rs.next()){
				Package tmp = new Package(rs.getInt("PackageID"), rs.getInt("deliveryClass"));
				System.out.println("loaded package");
				packList.add(tmp);
			}
			System.out.println("PackageList: " + packList.size());
			conn.commit();
			conn.close();
			return packList;
		}catch(Exception e){
			e.printStackTrace();
			return packList;
		}
		
	}
	//Gets all Contains entries that correspond to a given Package by ID
	public static ArrayList<Item> getPackageContent(int packID){
		ArrayList<Item> lst = new ArrayList<Item>();
		
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("SELECT Items.ItemID, Items.name, Items.weight, Items.volume, Items.fragile FROM Items INNER JOIN"
					+ " Contains ON Contains.ItemID = Items.ItemID"
					+ " WHERE PackageID = ?;");
			ps.setInt(1, packID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Item tmp = new Item(rs.getInt("ItemID"), rs.getString("name"), rs.getDouble("weight"), rs.getDouble("volume"), rs.getBoolean("fragile"));
				lst.add(tmp);
			}
			return lst;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	//Creates a new entry in the Items table using the given parameters
	public static int addItem(String name, double weight, double volume, boolean fragile){
		
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO Items(name, weight, volume, fragile) VALUES (?,?,?,?);");
			ps.setString(1, name);
			ps.setDouble(2, weight);
			ps.setDouble(3, volume);
			ps.setBoolean(4, fragile);
			
			ps.executeUpdate();
			
			Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery("SELECT last_insert_rowid();");
			int ID = rs.getInt(1);
			conn.commit();
			conn.close();
			return ID;
		}catch(Exception e){
			//return -1 to signify failed operation
			e.printStackTrace();
			return -1;
		}
		
	}
	
	//Creates new package entry using a given delivery class number
	public static int addPackage(int dClass){
		
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO Packages(deliveryClass) VALUES (?);");
			ps.setInt(1, dClass);
			ps.executeUpdate();
			Statement s = conn.createStatement();
			ResultSet rs = s.executeQuery("SELECT last_insert_rowid();");
			rs.next();
			int ID = rs.getInt(1);
			conn.commit();
			conn.close();
			return ID;
		}catch(Exception e){
			//return -1 to signify failed operation
			e.printStackTrace();
			return -1;
		}
	}

	//Gets route information of a given package by ID
	public static ResultSet getRoute(int ID){
		
		try{
			DatabaseHandler.conn = connect();
			
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Routes WHERE PackageID = ?;");
			ps.setInt(1, ID);
			return ps.executeQuery();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	//Write route information into database
	public static int setRoute(int ID, int start, int end){
		
		try{
			DatabaseHandler.conn = connect();
			
			PreparedStatement ps = conn.prepareStatement("DELETE FROM Routes WHERE PackageID = ?;");
			ps.setInt(1, ID);
			ps.executeUpdate();
			ps.close();
			
			PreparedStatement ps2 = conn.prepareStatement("INSERT INTO Routes(PackageID, StartID, DestID) VALUES (?, ?, ?);");
			ps2.setInt(1, ID);
			ps2.setInt(2, start);
			ps2.setInt(3, end);
			ps2.executeUpdate();
			conn.commit();
			conn.close();
			return 0;
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	
	//Creates an entry in the Contains table corresponding to the item being added into a package
	public static int addItemToPackage(int itemID, int packID){
		
		try{
			
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO Contains(ItemID, PackageID) VALUES (?,?);");
			ps.setInt(1, itemID);
			ps.setInt(2, packID);
			ps.executeUpdate();
			conn.commit();
			conn.close();
			return 0;
			
		}catch(Exception e){
			System.out.println("Database error.");
			e.printStackTrace();
			return -1;
		}
		
	}
	
	public static ResultSet getLog(){
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM EventInfo;");
			return ps.executeQuery();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	//adds log event
	public static int addEvent(int typeID){
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO EventLog(EventTypeID) VALUES (?);");
			ps.setInt(1, typeID);
			
			ps.executeUpdate();
			conn.commit();
			conn.close();
			return 0;
			
		}catch(Exception e){
			System.out.println("Database error.");
			e.printStackTrace();
			return -1;
		}
	}
	
	//Removes the Contains-table entry for a certain item and package
	public static int removeItemFromPackage(int itemID, int packID){
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM Contains WHERE ItemID = ? AND PackageID = ?");
			ps.setInt(1, itemID);
			ps.setInt(2, packID);
			ps.executeUpdate();
			conn.commit();
			conn.close();
			return 0;
		}catch(Exception e){
			System.out.println("Database error.");
			e.printStackTrace();
			return -1;
		}
	}
	
	//Delete package and all its contained items and routes
	//SQLite deletes all Route and package entries using foreign key constraints
	public static int deletePackage(int packID){
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM Packages WHERE PackageID = ?");
			ps.setInt(1,packID);
			ps.executeUpdate();
			conn.commit();
			conn.close();
			return 0;
		}catch(Exception e){
			System.out.println("Database error.");
			e.printStackTrace();
			return -1;
		}
	}
	
	//Delete Item and all of its instances in packages
	//SQLite removes all "contains" entries using foreign key constraints
	public static int deleteItem(int itemID){
		try{
			DatabaseHandler.conn = connect();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM Items WHERE ItemID = ?");
			ps.setInt(1,itemID);
			ps.executeUpdate();
			conn.commit();
			conn.close();
			return 0;
		}catch(Exception e){
			System.out.println("Database error.");
			e.printStackTrace();
			return -1;
		}
	}
	
	
	//Clears session specific information
	public static void clearSession(){
		
		try{
			DatabaseHandler.conn = connect();
			Statement s = conn.createStatement();
			s.executeUpdate("DELETE FROM Items;");
			s.executeUpdate("DELETE FROM EventLog;");
			s.executeUpdate("DELETE FROM Packages;");
			s.close();
			conn.commit();
			conn.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//Method for refreshing database from XML
	public static void clearDB(){

		try{
			DatabaseHandler.conn = connect();
			Statement s = conn.createStatement();
			s.executeUpdate("DELETE FROM SmartPost;");
			s.executeUpdate("DELETE FROM Location;");
			s.executeUpdate("DELETE FROM Metadata;");
			s.executeUpdate("DELETE FROM PostalCodes;");
			s.close();
			conn.commit();
			conn.close();		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
