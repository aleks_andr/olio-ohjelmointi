// Andrey Aleksandrov 0447608
package application;

public class SmartPost {
	//private variables
	private int ID;
	private String name;
	private String address;
	private String postalCode;
	private String city;
	private String availability;
	private double lat, lng;
	
	
	
//constructor method
	public SmartPost(String name, String postalcode, String address, String city, String avail, double lat, double lng){
		this.name = name;
		this.postalCode = postalcode;
		this.address = address;
		this.city = city;
		this.availability = avail;
		this.lat = lat;
		this.lng = lng;
	}

	
	public void setID(int ID){
		this.ID = ID;
	}
	
//getter-methods
	public int getID(){
		return this.ID;
	}
	
	public String getAddress(){
		return this.address;
	}
	
	public String getName(){
		return this.name;
	}

	public String getCode(){
		return this.postalCode;
	}
	
	public String getCity(){
		return this.city;
	}
	
	public String getAvail(){
		return this.availability;
	}
	
	public double getLat(){
		return this.lat;
	}
	
	public double getLng(){
		return this.lng;
	}
}
//---------------------