// Andrey Aleksandrov 0447608
package application;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class MainController implements Initializable{

	@FXML WebView mapView;
	@FXML ChoiceBox<String> locationMenu;
	@FXML ChoiceBox<String> deviceList;
	@FXML AnchorPane basePane;
	@FXML TextArea logField;
	public static Stage packageStage;
	public static PackageUI pUIcontroller;
//	private SmartPost start, end;
	
	@Override
	public void initialize(URL url, ResourceBundle res){
		DatabaseHandler.buildDb();
		PostOffices.buildList();
		if(isLoadRequired()){
			//Do nothing
		}else{
			//clear session specific data
			//delete all items, packages, contains, events
			DatabaseHandler.clearSession();
		}
	
		logField.setEditable(false);
		mapView.getEngine().load(getClass().getResource("index.html").toExternalForm());
		
		if(PostOffices.locations.size() == 0){
			XmlToDB.getInstance().getXML();
			PostOffices.buildList();
		}
		//Initializes storage
		Storage.getInstance();
		//adds listener to choicebox selection, allows for updating of second choicebox
		locationMenu.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				System.out.println("ChoiceBox event triggered.");
				
				Platform.runLater(() -> {
					
					loadCityLocs();
					
				});
				
			}
		});
		
		loadChoiceBox();
		DatabaseHandler.addEvent(1);
	}
	
	//loads cities into a ChoiceBox
	@FXML
	public void loadChoiceBox(){
		locationMenu.getItems().clear();
		
		for(SmartPost x : PostOffices.locations){
			if(!locationMenu.getItems().contains(x.getCity())){
				locationMenu.getItems().add(x.getCity());
			}
		}
		locationMenu.getSelectionModel().selectFirst();
		
	}
	
	//loads locations within selected city
	private void loadCityLocs(){
		String city = locationMenu.getSelectionModel().getSelectedItem();
		deviceList.getItems().clear();
		
		for(SmartPost x : PostOffices.locations){
			if(x.getCity().equals(city)){
				deviceList.getItems().add(x.getID() + ": " + x.getName() + ", " + x.getAddress());
			}
		}
		deviceList.getSelectionModel().selectFirst();
	}
	
	//Clears all markers from the map
	@FXML
	public void clearMarkers(){
		mapView.getEngine().executeScript("document.clearMarkers()");
	}
	
	
	//Clear all paths on map
	@FXML
	public void clearPaths(){
		mapView.getEngine().executeScript("document.deletePaths()");
	}
	
	@FXML
	private void fillDB(){
		DatabaseHandler.clearDB();
		XmlToDB.getInstance().getXML();
		PostOffices.locations.clear();
		PostOffices.buildList();
		loadChoiceBox();
	}
	
	//document.goToLocation(String address, String metadata, String COLOR)
	//address: street address, postalcode, city
	//draws a marker on the map based on the selected location
	@FXML
	public void drawLocs(){
		String name = deviceList.getSelectionModel().getSelectedItem();
		if(name == null){
			return;
		}
		SmartPost sp = PostOffices.getOfficeByID(Integer.parseInt(name.split(":")[0]));
		
		mapView.getEngine().executeScript("document.goToLocation(" + "\"" + sp.getAddress() + ", " + sp.getCode() + ", " + sp.getCity() + "\", \"" + sp.getName() + ", " + sp.getAvail() + "\"" + ", \"blue\"" + ");");
	}
		
	
	//Calls JavaScript method to get the distance between 2 SmartPosts
	public Double checkDistance(SmartPost start, SmartPost end){
		
		ArrayList<String> coords = new ArrayList<String>();
		coords.add(Double.toString(start.getLat()));
		coords.add(Double.toString(start.getLng()));
		coords.add(Double.toString(end.getLat()));
		coords.add(Double.toString(end.getLng()));
		return Double.parseDouble((String) mapView.getEngine().executeScript("document.getDistance(" + coords + ");"));
	}
	
	//document.createPath(ArrayList<String> coords, String color, int speed)
	//ArrayList 
	//Draw route using Package object to receive route
	//@FXML
	public void drawPath(Package pack){
		
		if(pack.getStart() == null || pack.getEnd()  == null){
			return;
		}
	
		ArrayList<String> coords = new ArrayList<String>();
		coords.add(Double.toString(pack.getStart().getLat()));
		coords.add(Double.toString(pack.getStart().getLng()));
		coords.add(Double.toString(pack.getEnd().getLat()));
		coords.add(Double.toString(pack.getEnd().getLng()));
		
		mapView.getEngine().executeScript("document.goToLocation(" + "\"" + pack.getStart().getAddress() + ", " + pack.getStart().getCode() + ", " + pack.getStart().getCity() + "\", \"" + pack.getStart().getName() + ", " + pack.getStart().getAvail() + "\"" + ", \"green\"" + ");");
		mapView.getEngine().executeScript("document.goToLocation(" + "\"" + pack.getEnd().getAddress() + ", " + pack.getEnd().getCode() + ", " + pack.getEnd().getCity() + "\", \"" + pack.getEnd().getName() + ", " + pack.getEnd().getAvail() + "\"" + ", \"red\"" + ");");

		
		System.out.println("document.createPath(" + coords + ", \"blue\", \"" +  pack.getPackageClass() +"\");");
		mapView.getEngine().executeScript("document.createPath(" + coords + ", \"blue\", \"" +  pack.getPackageClass() +"\");");
		DatabaseHandler.addEvent(6);
	}
	//Updates log tab
	@FXML
	public void updateLog(){
		logField.clear();
		try{
			ResultSet rs = DatabaseHandler.getLog();
			while(rs.next()){
				logField.appendText(rs.getString("dateTime") + "\t" + rs.getString("description") + "\n");
			}
		}catch(Exception e){
			e.printStackTrace();
			logField.clear();
			return;
		}
	}
	//Writes log to .txt file
	@FXML
	public void writeLogToFile(){
		
		try{
			DatabaseHandler.addEvent(11);
			ResultSet rs = DatabaseHandler.getLog();
			BufferedWriter bw = new BufferedWriter(new FileWriter(System.getProperty("user.home") + "/SmartPostLog.txt"));
			
			while(rs.next()){
				bw.write(rs.getString("dateTime") + "\t" + rs.getString("description") + "\n");
			}
			bw.close();
			updateLog();
			return;
		}catch(Exception e){
			e.printStackTrace();
			DatabaseHandler.addEvent(12);
			return;
		}
	}
	
	//Opens GUI for handling Packages and Items
	@FXML
	public void openPackageGUI(){
		if(packageStage != null){
			return;
		}
		
		try{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("packageMenu.fxml"));
			AnchorPane pane = loader.load();
			pUIcontroller = loader.getController();
			
			packageStage = new Stage();
			packageStage.setTitle("Pakettiasetukset");
			packageStage.setScene(new Scene(pane));
			
			packageStage.setOnCloseRequest(e -> {
				Platform.runLater(() -> {
					MainController.packageStage = null;
				});
			});
			packageStage.setResizable(false);
			packageStage.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	//Asks user if they want to continue their session or reset program state
	public boolean isLoadRequired(){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Ohjelman tilan/lokitiedon lataus.");
		alert.setHeaderText("Haluatko jatkaa ohjelman edellisestä tilasta?");
		alert.setContentText("Ladataanko ohjelman edellinen tila vai aloitetaanko alusta?");
		alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
		alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		alert.getDialogPane().getStyleClass().add("dialog");
		
		Optional<ButtonType> result = alert.showAndWait();
		if(result.get() == ButtonType.OK){
			return true;
		}else{
			return false;
		}
	}
}
