// Andrey Aleksandrov 0447608
package application;

import java.util.ArrayList;

public class PostOffices {

	public static ArrayList<SmartPost> locations;
	
	private PostOffices(){
		
	}
	//gets a list of locations from database
	public static void buildList(){
		PostOffices.locations = DatabaseHandler.getPostInfo();
	}
	
	//allows for getting a specific office by its ID
	public static SmartPost getOfficeByID(int ID){
		for (SmartPost x : locations){
			if (x.getID() == ID){
				return x;
			}
		}
		return null;
	}
}
