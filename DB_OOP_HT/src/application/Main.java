// Andrey Aleksandrov 0447608
package application;
	
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
//import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;



public class Main extends Application {
	public static MainController controller;
	
	@Override
	public void start(Stage primaryStage) {
		try {
					
			FXMLLoader loader = new FXMLLoader(getClass().getResource("guiFXML.fxml"));
			TabPane pane = loader.load();
			controller = loader.getController();
			//controller.setPackage(p)			
			
			
			Scene scene = new Scene(pane);
			primaryStage.setOnCloseRequest(e -> {
				DatabaseHandler.addEvent(2);
				Platform.exit();
			});
			
			primaryStage.setTitle("SmartPost Simulator 2017");
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
