// Andrey Aleksandrov 0447608
package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class PackageUI  implements Initializable{

	@FXML ChoiceBox<String> packageList;
	@FXML ChoiceBox<String> itemList;
	@FXML TextField itemNameInput, itemWeightInput, itemVolumeInput;
	@FXML Label itemInfo, routeInfo, classLabel;
	@FXML CheckBox itemFragileInput;
	@FXML ListView<String> containsListView;
	public static Stage routeStage;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
	//Add event	listener to changes in selection of ChoiceBox object
		
		packageList.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				//System.out.println("ChoiceBox event trigger.");
				
				Platform.runLater(() -> {
					updatePackageInfo();
				});
			}
		});
		
		itemList.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				//System.out.println("ItemList choicebox event");
				
				Platform.runLater(() -> {
					updateItemInfo();
				});
			}
		});
		//initialize ChoiceBox lists
		loadPackageList();
		loadItemList();
	}
	
	//display Packages in a ChoiceBox object
	@FXML
	public void loadPackageList(){
		packageList.getItems().clear();
		
		for(Package x : Storage.packages){
			packageList.getItems().add("Paketti: " + Integer.toString(x.getID()));
		}
		packageList.getSelectionModel().selectFirst();
	}	
	
	
	//Display Package's info in appropriate ListView objects
	@FXML
	public void updatePackageInfo(){
		containsListView.getItems().clear();
		routeInfo.setText("");
		int id;
		
		try{
			id = Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]);
		}catch(NullPointerException e){
			return;
		}
		Package pack = Storage.getPackage(id);
		
		
		
		for (Item i : pack.getItems()){
			containsListView.getItems().add(i.getID() + ": " + i.getName() + ", " + i.getWeight() + "kg");
		}
		
		try{
			routeInfo.setText(pack.getStart().getName() + " ->\n" + pack.getEnd().getName());
		}catch(NullPointerException e){
			System.out.println("Package route null.");
		}
		
		classLabel.setText("Luokka: " + pack.getPackageClass());
	}

	//Load appropriate information into ChoiceBox objects
	@FXML
	public void loadItemList(){
		itemList.getItems().clear();
		for (Item x : Storage.itemStock){
			itemList.getItems().add(x.getID() + ": " + x.getName());
		}
		itemList.getSelectionModel().selectFirst();
		//updateItemInfo();
	}
	
	@FXML
	public void callPath(){
		int id;
		Package pack;
		
		try{
			id = Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]);
			pack = Storage.getPackage(id);
		}catch(NullPointerException e){
			return;
		}
		Main.controller.drawPath(pack);
	}
	
	
	@FXML
	public void updateItemInfo(){
		int ID;
		try{
			ID = Integer.parseInt(itemList.getSelectionModel().getSelectedItem().split(":")[0]);
		}catch(NullPointerException e){
			return;
		}
		
		for (Item x : Storage.itemStock){
			if(x.getID() == ID){
				itemInfo.setText(x.getID() + ": " + x.getName()+ ", " + x.getWeight() + "kg, " + x.getVol() + "l");
				return;
			}
		}
		itemInfo.setText("");
	}
	//------------------------------------------------------------------
	
	//Creates new item with specified parameters, and adds item to database
	@FXML
	public void createItem(){
		String name = itemNameInput.getText();
		Double weight = null;
		Double volume = null;
		boolean fragile = itemFragileInput.isSelected();
		try{
			weight = Double.parseDouble(itemWeightInput.getText());
			volume = Double.parseDouble(itemVolumeInput.getText());
		}catch(NumberFormatException e){
			//System.out.println("Could not convert one or more numbers");
			alertMissingFields();
			return;
		}
		
		if(!name.isEmpty() && (weight != null) && (volume != null)){
			System.out.println("Accepted input");
		}else{
			//System.out.println("Not all fields are correct");
			alertMissingFields();
			return;
		}
		
		int ID;
		
		if((ID = DatabaseHandler.addItem(name, weight, volume, fragile)) == -1){
			System.out.println("Database error.");
			return;
		}else{
			Item tmp = new Item(ID, name, weight, volume, fragile);
			Storage.itemStock.add(tmp);
			if(DatabaseHandler.addEvent(5) == -1){
				alertFailedOperation();
			}
			loadItemList();
		}
		
	}
	
	//Insert a selected Item into package, update database accordingly
	@FXML
	public void addItemToPackage(){
		
		if (itemList.getSelectionModel().getSelectedItem() == null || packageList.getSelectionModel().getSelectedItem() == null){
			System.out.println("1 or 2 invalid selections.");
			return;
		}
		
		
		
		int itemID = Integer.parseInt(itemList.getSelectionModel().getSelectedItem().split(":")[0]);
		int packID = Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]);
		Package pack = Storage.getPackage(packID);
		
		
		
		
		for (Item x : pack.getItems()){
			if (x.getID() == itemID){
				//Item already inside selected package
				alertItemInsertionFail("Esine on jo paketissa.");
				System.out.println("Item already inside selected package. Select a different Item.");
				return;
			}
		}
		
		String error;
		if(!((error = pack.checkItem(Storage.getItem(itemID))) == null)){
			alertItemInsertionFail(error);
			return;
		}
		
		
		if (DatabaseHandler.addItemToPackage(itemID, packID) != -1){
			//entry added to database successfully
			System.out.println("Added item " + itemID + " to package " + packID);
			pack.getItems().add(Storage.getItem(itemID));
			if(DatabaseHandler.addEvent(7) == -1){
				alertFailedOperation();
			}
			updatePackageInfo();
			return;
		}else{
			alertFailedOperation();
			//System.out.println("Item didnt fit inside package, or a database error occurred");
		}	
	}
	
	
	//Creates new package and adds it to database (class selected by user)
	@FXML
	public void createPackage(){
		ArrayList<String> choices = new ArrayList<>();
		choices.add("1. luokka");
		choices.add("2. luokka");
		choices.add("3. luokka");
		
		ChoiceDialog<String> dialog = new ChoiceDialog<>("1. luokka", choices);
		
		dialog.setTitle("Uusi paketti");
		dialog.setHeaderText("Valitse paketin lähetysluokka");
		dialog.setContentText("Valitse luokka: ");
		
		dialog.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		dialog.getDialogPane().getStyleClass().add("dialog");
		
		dialog.initOwner(MainController.packageStage);
		Optional<String> result = dialog.showAndWait();
		if(!result.isPresent()){
			return;
		}
		//System.out.println(result.get());
		int packClass =  Integer.parseInt(result.get().split(". ")[0]);
		int ID;
		
		if((ID = DatabaseHandler.addPackage(packClass)) == -1){
			alertFailedOperation();
			return;
		}
		Storage.packages.add(new Package(ID, packClass));
		if(DatabaseHandler.addEvent(4) == -1){
			alertFailedOperation();
		}
		loadPackageList();
	}
	
	//remove selected item from package, update database accordingly
	@FXML
	public void removeItem(){
		ArrayList<Item> tmp_toDel = new ArrayList<Item>();
		//System.out.println(containsListView.getSelectionModel().getSelectedItem());
		
		if (containsListView.getSelectionModel().getSelectedItem() == null || packageList.getSelectionModel().getSelectedItem() == null){
			return;
		}
		
		int itemID = Integer.parseInt(containsListView.getSelectionModel().getSelectedItem().split(":")[0]);
		int packID = Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]);
		
		
		if(DatabaseHandler.removeItemFromPackage(itemID, packID) == -1){
			alertFailedOperation();
			return;
		}
		
		for(Item x : Storage.getPackage(packID).getItems()){
			if(x.getID() == itemID){
				tmp_toDel.add(x);
			}
		}
			
		for(Item x : tmp_toDel){
			Storage.getPackage(packID).getItems().remove(x);		
		}
		
		if(DatabaseHandler.addEvent(8) == -1){
			alertFailedOperation();
		}
		updatePackageInfo();
	}
	
	@FXML
	public void deletePackage(){
		int packID;
		Package tmp;
		
		try{
			packID = Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]);
			tmp = Storage.getPackage(packID);
		}catch(NullPointerException e){
			return;
		}
		
		if(DatabaseHandler.deletePackage(packID) == -1){
			alertFailedOperation();
			return;
		}
		
		Storage.packages.remove(tmp);
		
		if(DatabaseHandler.addEvent(10) == -1){
			alertFailedOperation();
		}
		
		loadPackageList();
	}

	//Delete selected item
	@FXML
	public void deleteItem(){
		int itemID;
		Item tmp;
		try{
			itemID = Integer.parseInt(itemList.getSelectionModel().getSelectedItem().split(":")[0]);
			tmp = Storage.getItem(itemID);
		}catch(NullPointerException e){
			return;
		}
		
		if(DatabaseHandler.deleteItem(itemID) == -1){
			alertFailedOperation();
			return;
		}
		
		for(Package x : Storage.packages){
			for(int i = 0; i < x.getItems().size(); i++){
				if(x.getItems().get(i).getID() == itemID){
					x.getItems().remove(i);
				}
			}
		}
		Storage.itemStock.remove(tmp);
		
		if(DatabaseHandler.addEvent(9) == -1){
			alertFailedOperation();
		}
		loadItemList();
		updatePackageInfo();
	}
	
	
//Opens route settings menu for selected package
	@FXML
	public void routeSettings(){
		//make sure only a single instance of the window may be open at once to avoid conflict
		if(PackageUI.routeStage != null){
			return;
		}
		
		try{
			Package p = Storage.getPackage(Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]));
			FXMLLoader loader = new FXMLLoader(getClass().getResource("routeMenu.fxml"));
			AnchorPane pane = loader.load();
			RouteUI controller = loader.getController();
			controller.setPackage(p);
			PackageUI.routeStage = new Stage();
			PackageUI.routeStage.setOnCloseRequest(e -> {
					PackageUI.routeStage = null;
			});
			PackageUI.routeStage.setTitle("Reittiasetukset");
			PackageUI.routeStage.setScene(new Scene(pane));
			PackageUI.routeStage.setResizable(false);
			PackageUI.routeStage.show();
		}catch(NullPointerException e){
			System.out.println("nullpointer");
			return;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	//Alert syntax source: https://docs.oracle.com/javase/8/javafx/api/javafx/scene/control/Alert.html
	//Alert about item not fitting into package
	public static void alertItemInsertionFail(String error){
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Esineen lisääminen epäonnistui.");
		alert.setHeaderText("Esineen lisääminen pakettiin epäonnistui.");
		alert.setContentText("Virheilmoitus: " + error);
		alert.showAndWait();
	}
	
	//Alert about restrictions
	
	public void restrictionInfoAlert(){
		int packID;
		Package tmp;
		
		try{
			packID = Integer.parseInt(packageList.getSelectionModel().getSelectedItem().split(" ")[1]);
			tmp = Storage.getPackage(packID);
		}catch(NullPointerException e){
			return;
		}
		
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setResizable(true);
		alert.setTitle("Paketin toimitusluokan rajoitukset:");
		alert.setHeaderText("Luokan " + tmp.getPackageClass() + " rajoitukset");
		alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		alert.getDialogPane().getStyleClass().add("dialog");
		TextArea textArea = new TextArea();
		textArea.setEditable(false);
		
	
		
		textArea.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane content = new GridPane();
		content.setMaxWidth(Double.MAX_VALUE);
		content.add(textArea, 0, 0);
		alert.getDialogPane().setContent(content);
		
		textArea.appendText("Luokka: " + tmp.getPackageClass() + "\n");
		textArea.appendText("Etäisyysraja: " + tmp.getRange() + "km\n");
		textArea.appendText("Painoraja: " + tmp.getMaxWeight() + "kg\n");
		textArea.appendText("Tilavuusraja: " + tmp.getMaxVolume() + "l\n");
		textArea.appendText("Särkyvä: " + tmp.isFragile() + "\n");
		
		alert.showAndWait();
	}
	
	
	//Alert about failed operation
	@FXML
	public void alertFailedOperation(){
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Operaatio ei onnistunut.");
		alert.setHeaderText("Tietokantavirhe.");
		alert.setContentText("Tietojen lisäämisessä/lukemisessa tapahtui virhe. Yritä uudelleen.");
		alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		alert.getDialogPane().getStyleClass().add("dialog");
		DatabaseHandler.addEvent(3);
		alert.showAndWait();
	}
	
	//Incorrect input alert
	@FXML
	public void alertMissingFields(){
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Puuttuvat/virheelliset kentät.");
		alert.setHeaderText("Syötit yhden tai useamman kelvottoman arvon.");
		alert.setContentText("Syöte ei kelvannut. Esinettä ei luotu. Yritä uudelleen.");
		alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		alert.getDialogPane().getStyleClass().add("dialog");
		alert.showAndWait();
	}
}
