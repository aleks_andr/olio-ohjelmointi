package application;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XmlToDB {
	
	private static XmlToDB instance;
	

	public static XmlToDB getInstance(){
		if(XmlToDB.instance == null){
			XmlToDB.instance = new XmlToDB();
			return instance;
		}else{
			return instance;
		}
	}
	
	
	public void getXML(){
		long startTime = System.nanoTime();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try{
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.parse("http://smartpost.ee/fi_apt.xml");
			
			Element doc = dom.getDocumentElement();
			NodeList nl = doc.getElementsByTagName("place");

			for (int i = 0;i < nl.getLength(); i++){
				Element elem = (Element) nl.item(i);
				
				SmartPost sp = getSmartPost(elem);
				//ADD SmartPost-object to database//
				DatabaseHandler.addPost(sp);
			}
			DatabaseHandler.conn.commit();
			DatabaseHandler.conn.close();
			System.out.println("Time taken: " + (System.nanoTime()-startTime)/1000000 + "ms");
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	private SmartPost getSmartPost(Element elem){
		String name = getTextValue(elem, "postoffice");
		String city = getTextValue(elem, "city");
		String address = getTextValue(elem, "address");
		String postalcode = getTextValue(elem, "code");
		String avail = getTextValue(elem, "availability");
		
		double lat = Double.parseDouble(getTextValue(elem, "lat"));
		double lng = Double.parseDouble(getTextValue(elem, "lng"));

		return new SmartPost(name, postalcode, address, city, avail, lat, lng);
	}
	
	private String getTextValue(Element el, String tag){
		String value = null;
		NodeList nl = el.getElementsByTagName(tag);
		if(nl != null && nl.getLength() > 0){
			Element elem = (Element) nl.item(0);
			value = elem.getFirstChild().getNodeValue();
		}
		
		return value;
	}
	
}
