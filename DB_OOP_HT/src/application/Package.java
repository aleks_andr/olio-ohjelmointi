// Andrey Aleksandrov 0447608
package application;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Package {

	protected int ID;
	protected boolean fragile = false;
	protected double maxWeight;
	protected double maxVolume;
	protected int maxRange;
	protected int classNum;
	
	private SmartPost start, end;
	
	private ArrayList<Item> content = new ArrayList<Item>();
	
	public Package(int ID, int packageClass){
		this.ID = ID;
		this.classNum = packageClass;
		//Dynamically assign delivery restrictions based on the restrictions specified in the database
		//Slightly lowers performance of creating an object but is safer in terms of information integrity and flexibility
		ResultSet rs = DatabaseHandler.getClassRestrictions(packageClass);
		try{
			//System.out.println("loading limits for package: " + ID);
			rs.next();
			this.maxWeight = rs.getDouble("maxWeight");
			this.fragile = rs.getBoolean("fragile");
			this.maxVolume = rs.getDouble("maxVolume");
			this.maxRange = rs.getInt("maxRange");
		}catch(Exception e){
			//System.out.println("exception during loading of restrictions");
			e.printStackTrace();
		}
		//Checks package for contents and route (from the database)
		this.content = DatabaseHandler.getPackageContent(ID);
		ResultSet rs2 = DatabaseHandler.getRoute(ID);
		try{
			rs2.next();
			this.start = PostOffices.getOfficeByID(rs2.getInt("StartID"));
			this.end = PostOffices.getOfficeByID(rs2.getInt("DestID"));

			System.out.println("Route loaded for package: " + this.getID());
		}catch(Exception e){
			System.out.println("No route found for package: " + this.getID());
			//e.printStackTrace();
		}
		
		
	}
	

//Getter methods
	public ArrayList<Item> getItems(){
		return this.content;
	}
	
	public int getID(){
		return this.ID;
	}
	
	public SmartPost getStart(){
		return this.start;
	}
	
	public SmartPost getEnd(){
		return this.end;
	}
	
	public boolean isFragile(){
		return this.fragile;
	}
	
	public int getPackageClass(){
		return this.classNum;
	}
	
	public int getRange(){
		return this.maxRange;
	}
	
	public double getMaxWeight(){
		return this.maxWeight;
	}
	
	public double getMaxVolume(){
		return this.maxVolume;
	}
//--------------------------------
	
	public void setRoute(SmartPost start, SmartPost end){
		this.start = start;
		this.end = end;
	}
	

	public String checkItem(Item item){
		double curr_weight = 0;
		double curr_volume = 0;
		//Calculate current weight and volume of items in package
		
		for (Item x : this.content){
			curr_weight += x.getWeight();
			curr_volume += x.getVol();
		}
		
		if(curr_weight + item.getWeight() > this.maxWeight){
			//No space left to add Item
			System.out.println("Weight limitations dont allow this item to be added.");
			return "Painoraja ylittyy.";
		}else if(curr_volume + item.getVol() > this.maxVolume){
			//No space to add Item
			System.out.println("Item will not fit in the package.");
			return "Tilavuusraja ylittyy";
		}else{
			
			if(item.isFragile() && this.fragile){
				//fine
			}else if(item.isFragile() && !this.fragile){
				//item will break
				return "Esine menee rikki kuljetuksessa.";
			}else{}
			
			
			System.out.println("Item added.");
			//this.content.add(item);
			return null;
		}
	}

//checks if package-specific distance limitations are not surpassed
//return true if route distance meets the requirements, false otherwise
	public boolean checkDistanceLimit(int dist){
		
		if (dist > this.maxRange){
			return false;
		}else{
			return true;
		}
	}
}
