// Andrey Aleksandrov 0447608
package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class RouteUI implements Initializable{

	
	@FXML ChoiceBox<String> startCity, startLoc, endCity, endLoc;
	private Package selectedPackage;
	private Double distance = (double) 0;
	@FXML Label titleLabel;
	@FXML Label distLabel;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		//Make this window close when its parent is closed
		MainController.packageStage.setOnCloseRequest(e -> {
			
			if(PackageUI.routeStage != null){
			
				PackageUI.routeStage.close();
				PackageUI.routeStage = null;
				MainController.packageStage = null;
				
			}else{
				MainController.packageStage = null;
			}
		});
		
		//Create event listeners to update UI when the value of ChoiceBox gets changed
		startCity.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				System.out.println("ChoiceBox event triggered.");
				Platform.runLater(() -> {
					loadStartCity();
				});	
			}
		});
		
		endCity.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>(){
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				System.out.println("ChoiceBox event triggered.");
				Platform.runLater(() -> {
					loadEndCity();
				});
				
			}
		});
		
		cityBoxes();
		
		startLoc.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				Platform.runLater(() -> {
					updateDistance();
				});
			}
		});
		
		endLoc.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number n1, Number n2){
				Platform.runLater(() -> {
					updateDistance();
				});
			}
		});	
	//----------------------------------------------------------------------------------------------------------
	}
	
	public void setPackage(Package p){
		this.selectedPackage = p;
		this.titleLabel.setText("Paketin " + p.getID() + " reittiasetukset:");
	}
	
	//Takes given locations and adds the route to selected package
	@FXML
	public void setRoute(){
		SmartPost start = PostOffices.getOfficeByID(Integer.parseInt((startLoc.getSelectionModel().getSelectedItem().split(":")[0])));
		SmartPost end = PostOffices.getOfficeByID(Integer.parseInt((endLoc.getSelectionModel().getSelectedItem().split(":")[0])));

		
		if(start.getID() == end.getID()){
			//no reason sending package between the same location
			routeAlert("Valitse eroavat sijainnit.");
			return;
		}
		
		if((Double) Main.controller.checkDistance(start, end) > selectedPackage.getRange()){
			//package cannot be sent that far
			routeAlert("Pakettia ei voida lähettää niin pitkälle.");
			return;
		}
		//Attempt to insert new Route into database
		if(DatabaseHandler.setRoute(this.selectedPackage.getID(), start.getID(), end.getID()) == -1){
			//failed operation alerts user and cancels the operation
			MainController.pUIcontroller.alertFailedOperation();
			return;
		}
		
		this.selectedPackage.setRoute(start, end);
		MainController.pUIcontroller.updatePackageInfo();
		Platform.runLater(() -> {
			PackageUI.routeStage.close();
			PackageUI.routeStage = null;
		});
		
	}
	
	//update distance label
	@FXML
	public void updateDistance(){
		SmartPost start = PostOffices.getOfficeByID(Integer.parseInt((startLoc.getSelectionModel().getSelectedItem().split(":")[0])));
		SmartPost end = PostOffices.getOfficeByID(Integer.parseInt((endLoc.getSelectionModel().getSelectedItem().split(":")[0])));
		
		if(start == null || end == null){
			return;
		}
		
		if(start.getID() == end.getID()){
			distLabel.setText("Etäisyys: 0 km");
			distance = 0.0;
			return;
		}
		
		System.out.println(Main.controller.checkDistance(start, end));
		distance = (Double) Main.controller.checkDistance(start, end);
		distLabel.setText("Etäisyys: " + distance + "km");
	
	}
	
	
	//Populates city ChoiceBoxes
	@FXML
	public void cityBoxes(){
		startCity.getItems().clear();
		endCity.getItems().clear();
		
		for(SmartPost x : PostOffices.locations){
			if(!startCity.getItems().contains(x.getCity())){
				startCity.getItems().add(x.getCity());
			}
			
			if(!endCity.getItems().contains(x.getCity())){
				endCity.getItems().add(x.getCity());
			}
		}
		startCity.getSelectionModel().selectFirst();
		endCity.getSelectionModel().selectFirst();
	}
	
	
	//Loads starting locations once city is selected
	@FXML
	public void loadStartCity(){
		startLoc.getItems().clear();
		
		for (SmartPost x : PostOffices.locations){
			if(x.getCity().equals(startCity.getSelectionModel().getSelectedItem())){
				startLoc.getItems().add(x.getID() + ": " + x.getName());
			}
		}
		startLoc.getSelectionModel().selectFirst();
	}
	
	//Loads destination locations once city is selected
	@FXML
	public void loadEndCity(){
		endLoc.getItems().clear();
		
		for (SmartPost x : PostOffices.locations){
			if(x.getCity().equals(endCity.getSelectionModel().getSelectedItem())){
				endLoc.getItems().add(x.getID() + ": " + x.getName());
			}
		}
		endLoc.getSelectionModel().selectFirst();
	}
	//Alert user about an incorrect route selection
	private void routeAlert(String error){
		
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Reitti ei kelpaa.");
		alert.setHeaderText("Valittu reitti ei kelpaa.");
		alert.setContentText("Syy: " + error);
		alert.getDialogPane().getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		alert.getDialogPane().getStyleClass().add("dialog");
		alert.showAndWait();
	}
}
