//Andrey Aleksandrov 0447608
package application;

public class Item {
	
	private int ID;
	private String name;
	private double weight;	//kg
	private double volume;  //l
	private boolean fragile; // 
	
	public Item(int ID,String n, double w, double v, boolean fragile){
		this.ID = ID;
		this.name = n;
		this.weight = w;
		this.volume = v;
		this.fragile = fragile;
	}
	
//Getters	
	public int getID(){
		return this.ID;
	}
	
	public String getName(){
		return this.name;
	}
	
	public double getWeight(){
		return this.weight;
	}

	public double getVol(){
		return this.volume;
	}

	public boolean isFragile(){
		return this.fragile;
	}
//-------------------------
}

