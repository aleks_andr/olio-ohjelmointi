// Andrey Aleksandrov 0447608
package application;

import java.util.ArrayList;

//Class used for handling Package and Item objects
public class Storage {
	//ArrayLists to act like storage for packages and all created items
	public static ArrayList<Item> itemStock = new ArrayList<Item>();
	public static ArrayList<Package> packages = new ArrayList<Package>();
	
	//Singleton-based Storage class
	private static Storage instance;
	
	private Storage(){
		itemStock = DatabaseHandler.getItemInfo();
		packages = DatabaseHandler.getPackages();
	}
	public static Storage getInstance(){
		if(instance != null){
			return instance;
		}else{
			instance = new Storage();
			return instance;
		}
	}
	//----------------------------------
	
	//getters
	public static Package getPackage(int ID){
		for (Package x : packages){
			if (x.getID() == ID){
				return x;
			}
		}
		return null;
	}
	
	public static Item getItem(int ID){
		for (Item x : itemStock){
			if (x.getID() == ID){
				return x;
			}
		}
		return null;
	}
	
}
